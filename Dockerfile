#
# Build stage
#
FROM maven:3.6.3-jdk-8 AS build
COPY . .
RUN mvn clean package -Pprod -DskipTests

#
# Package stage
#
# Use the official Java 8 image as the base image
FROM openjdk:8-jdk-slim

## Set the working directory
#WORKDIR /app

# Copy the Spring Boot JAR file into the container
COPY --from=build /target/SQA-0.0.1-SNAPSHOT.jar SQA.jar

# Expose the port that the application will run on
EXPOSE 8080

# Run the Spring Boot application
ENTRYPOINT ["java","-jar","SQA.jar"]
