package com.example.SQA.service.impl;

import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.repository.ConfigInsuranceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConfigInsuranceServiceImplTest {

    @Mock
    private ConfigInsuranceRepository mockConfigInsuranceRepository;

    @InjectMocks
    private ConfigInsuranceServiceImpl configInsuranceServiceImplUnderTest;

    @Test
    void testGetAllConfig() {
        // Setup
        final ConfigInsuranceEntity configInsuranceEntity = new ConfigInsuranceEntity();
        configInsuranceEntity.setId(0L);
        configInsuranceEntity.setPremiumRate(0.0f);
        configInsuranceEntity.setIsNomal(0);
        configInsuranceEntity.setIsFamily(0);
        configInsuranceEntity.setPeopleNum(0);
        configInsuranceEntity.setIsStudent(0);
        configInsuranceEntity.setIsPoorHousehold(0);
        final List<ConfigInsuranceEntity> expectedResult = Arrays.asList(configInsuranceEntity);

        // Configure ConfigInsuranceRepository.findAll(...).
        final ConfigInsuranceEntity configInsuranceEntity1 = new ConfigInsuranceEntity();
        configInsuranceEntity1.setId(0L);
        configInsuranceEntity1.setPremiumRate(0.0f);
        configInsuranceEntity1.setIsNomal(0);
        configInsuranceEntity1.setIsFamily(0);
        configInsuranceEntity1.setPeopleNum(0);
        configInsuranceEntity1.setIsStudent(0);
        configInsuranceEntity1.setIsPoorHousehold(0);
        final List<ConfigInsuranceEntity> configInsuranceEntities = Arrays.asList(configInsuranceEntity1);
        when(mockConfigInsuranceRepository.findAll()).thenReturn(configInsuranceEntities);

        // Run the test
        final List<ConfigInsuranceEntity> result = configInsuranceServiceImplUnderTest.getAllConfig();

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetAllConfig_ConfigInsuranceRepositoryReturnsNoItems() {
        // Setup
        when(mockConfigInsuranceRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<ConfigInsuranceEntity> result = configInsuranceServiceImplUnderTest.getAllConfig();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testChangeConfig() {
        // Setup
        final ConfigInsuranceEntity configInsuranceEntity = new ConfigInsuranceEntity();
        configInsuranceEntity.setId(0L);
        configInsuranceEntity.setPremiumRate(0.0f);
        configInsuranceEntity.setIsNomal(0);
        configInsuranceEntity.setIsFamily(0);
        configInsuranceEntity.setPeopleNum(0);
        configInsuranceEntity.setIsStudent(0);
        configInsuranceEntity.setIsPoorHousehold(0);

        final ConfigInsuranceEntity expectedResult = new ConfigInsuranceEntity();
        expectedResult.setId(0L);
        expectedResult.setPremiumRate(0.0f);
        expectedResult.setIsNomal(0);
        expectedResult.setIsFamily(0);
        expectedResult.setPeopleNum(0);
        expectedResult.setIsStudent(0);
        expectedResult.setIsPoorHousehold(0);

        when(mockConfigInsuranceRepository.existsById(0L)).thenReturn(false);

        // Configure ConfigInsuranceRepository.save(...).
        final ConfigInsuranceEntity configInsuranceEntity1 = new ConfigInsuranceEntity();
        configInsuranceEntity1.setId(0L);
        configInsuranceEntity1.setPremiumRate(0.0f);
        configInsuranceEntity1.setIsNomal(0);
        configInsuranceEntity1.setIsFamily(0);
        configInsuranceEntity1.setPeopleNum(0);
        configInsuranceEntity1.setIsStudent(0);
        configInsuranceEntity1.setIsPoorHousehold(0);
        when(mockConfigInsuranceRepository.save(new ConfigInsuranceEntity())).thenReturn(configInsuranceEntity1);

        // Run the test
        final ConfigInsuranceEntity result = configInsuranceServiceImplUnderTest.changeConfig(0L,
                configInsuranceEntity);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
        verify(mockConfigInsuranceRepository).save(new ConfigInsuranceEntity());
    }

    @Test
    void testGetCongigById() {
        // Setup
        final ConfigInsuranceEntity expectedResult = new ConfigInsuranceEntity();
        expectedResult.setId(0L);
        expectedResult.setPremiumRate(0.0f);
        expectedResult.setIsNomal(0);
        expectedResult.setIsFamily(0);
        expectedResult.setPeopleNum(0);
        expectedResult.setIsStudent(0);
        expectedResult.setIsPoorHousehold(0);

        // Configure ConfigInsuranceRepository.findById(...).
        final ConfigInsuranceEntity configInsuranceEntity1 = new ConfigInsuranceEntity();
        configInsuranceEntity1.setId(0L);
        configInsuranceEntity1.setPremiumRate(0.0f);
        configInsuranceEntity1.setIsNomal(0);
        configInsuranceEntity1.setIsFamily(0);
        configInsuranceEntity1.setPeopleNum(0);
        configInsuranceEntity1.setIsStudent(0);
        configInsuranceEntity1.setIsPoorHousehold(0);
        final Optional<ConfigInsuranceEntity> configInsuranceEntity = Optional.of(configInsuranceEntity1);
        when(mockConfigInsuranceRepository.findById(0L)).thenReturn(configInsuranceEntity);

        // Run the test
        final ConfigInsuranceEntity result = configInsuranceServiceImplUnderTest.getCongigById(0L);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetCongigById_ConfigInsuranceRepositoryReturnsAbsent() {
        // Setup
        when(mockConfigInsuranceRepository.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> configInsuranceServiceImplUnderTest.getCongigById(0L))
                .isInstanceOf(RuntimeException.class);
    }
}
