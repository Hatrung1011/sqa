package com.example.SQA.service.impl;

import com.example.SQA.entity.Users;
import com.example.SQA.model.BaseResponse;
import com.example.SQA.model.LoginDataModel;
import com.example.SQA.model.LoginModel;
import com.example.SQA.model.SignUpModel;
import com.example.SQA.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServicesImplTest {

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserServicesImpl userServicesImplUnderTest;

    @Test
    void testLogin() {
        // Setup
        final LoginModel loginModel = new LoginModel("username", "password");
        final LoginDataModel expectedResult = new LoginDataModel(1, "Login success", "name");

        // Configure UserRepository.findByUsername(...).
        final Users users = new Users();
        users.setId(0L);
        users.setUsername("username");
        users.setPassword("password");
        users.setHoTen("Demo");
        users.setIsActive(0);
        users.setIsDelete(0);
        users.setRole("user");
        users.setHoTen("name");
        when(mockUserRepository.findByUsername("username")).thenReturn(users);

        // Run the test
        final LoginDataModel result = userServicesImplUnderTest.login(loginModel);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testLoginWrongPassword() {
        // Setup
        final LoginModel loginModel = new LoginModel("username", "password123");
        final LoginDataModel expectedResult = new LoginDataModel(0, "Password is not correct", "");

        // Configure UserRepository.findByUsername(...).
        final Users users = new Users();
        users.setId(0L);
        users.setUsername("username");
        users.setPassword("password");
        users.setHoTen("Demo");
        users.setIsActive(0);
        users.setIsDelete(0);
        users.setRole("user");
        users.setHoTen("name");
        when(mockUserRepository.findByUsername("username")).thenReturn(users);

        // Run the test
        final LoginDataModel result = userServicesImplUnderTest.login(loginModel);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testLogin_UserRepositoryReturnsNull() {
        // Setup
        final LoginModel loginModel = new LoginModel("username", "password");
        final LoginDataModel expectedResult = new LoginDataModel(0, "User is not exist", "");
        when(mockUserRepository.findByUsername("username")).thenReturn(null);

        // Run the test
        final LoginDataModel result = userServicesImplUnderTest.login(loginModel);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testSignup() {
        // Setup
        final SignUpModel signUpModel = new SignUpModel("username", "password", "confirmPass", "name");
        final BaseResponse expectedResult = new BaseResponse(0, "Password and confirm password does not matching");

        // Configure UserRepository.findByUsername(...).
        final Users users = new Users();
        users.setId(0L);
        users.setUsername("username");
        users.setPassword("password");
        users.setHoTen("");
        users.setIsActive(0);
        users.setIsDelete(0);
        users.setRole("user");
        users.setHoTen("name");
        when(mockUserRepository.findByUsername("username")).thenReturn(users);

        // Configure UserRepository.save(...).
        final Users users1 = new Users();
        users1.setId(0L);
        users1.setUsername("username");
        users1.setPassword("password");
        users1.setHoTen("");
        users1.setIsActive(0);
        users1.setIsDelete(0);
        users1.setRole("user");
        users1.setHoTen("name");
        when(mockUserRepository.save(new Users())).thenReturn(users1);

        // Run the test
        final BaseResponse result = userServicesImplUnderTest.signup(signUpModel);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
        verify(mockUserRepository).save(new Users());
    }

    @Test
    void testSignup_UserRepositoryFindByUsernameReturnsNull() {
        // Setup
        final SignUpModel signUpModel = new SignUpModel("username", "password", "confirmPass", "name");
        final BaseResponse expectedResult = new BaseResponse(0, "Password and confirm password does not matching");
        when(mockUserRepository.findByUsername("username")).thenReturn(null);

        // Configure UserRepository.save(...).
        final Users users = new Users();
        users.setId(0L);
        users.setUsername("username");
        users.setPassword("password");
        users.setHoTen("");
        users.setIsActive(0);
        users.setIsDelete(0);
        users.setRole("user");
        users.setHoTen("name");
        when(mockUserRepository.save(new Users())).thenReturn(users);

        // Run the test
        final BaseResponse result = userServicesImplUnderTest.signup(signUpModel);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
        verify(mockUserRepository).save(new Users());
    }
}
