package com.example.SQA.service.impl;

import com.example.SQA.entity.BaseSalary;
import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.entity.Employee;
import com.example.SQA.repository.BaseSalaryRepository;
import com.example.SQA.repository.ConfigInsuranceRepository;
import com.example.SQA.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    private EmployeeRepository mockEmployeeRepository;
    @Mock
    private ConfigInsuranceRepository mockConfigInsuranceRepository;
    @Mock
    private BaseSalaryRepository mockBaseSalaryRepository;

    @InjectMocks
    private EmployeeServiceImpl employeeServiceImplUnderTest;

    @Test
    void testGetAllEmployees() {
        // Setup
        final Employee employee = new Employee();
        employee.setId(0L);
        employee.setName("name");
        employee.setSalary(0.0);
        final List<Employee> expectedResult = Arrays.asList(employee);

        // Configure EmployeeRepository.findAll(...).
        final Employee employee1 = new Employee();
        employee1.setId(0L);
        employee1.setName("name");
        employee1.setSalary(0.0);
        final List<Employee> employees = Arrays.asList(employee1);
        when(mockEmployeeRepository.findAll()).thenReturn(employees);

        // Run the test
        final List<Employee> result = employeeServiceImplUnderTest.getAllEmployees();

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetAllEmployees_EmployeeRepositoryReturnsNoItems() {
        // Setup
        when(mockEmployeeRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<Employee> result = employeeServiceImplUnderTest.getAllEmployees();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testCreateEmployee() {
        // Setup
        final Employee employee = new Employee();
        employee.setId(0L);
        employee.setName("name");
        employee.setSalary(0.0);

        final Employee expectedResult = new Employee();
        expectedResult.setId(0L);
        expectedResult.setName("name");
        expectedResult.setSalary(0.0);

        // Configure EmployeeRepository.save(...).
        final Employee employee1 = new Employee();
        employee1.setId(0L);
        employee1.setName("name");
        employee1.setSalary(0.0);
        when(mockEmployeeRepository.save(new Employee())).thenReturn(employee1);

        // Run the test
        final Employee result = employeeServiceImplUnderTest.createEmployee(employee);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetEmployeeById() {
        // Setup
        final Employee expectedResult = new Employee();
        expectedResult.setId(0L);
        expectedResult.setName("name");
        expectedResult.setSalary(0.0);

        // Configure EmployeeRepository.findById(...).
        final Employee employee = new Employee();
        employee.setId(0L);
        employee.setName("name");
        employee.setSalary(0.0);
        final Optional<Employee> optionalEmployee = Optional.of(employee);
        when(mockEmployeeRepository.findById(0L)).thenReturn(optionalEmployee);

        // Run the test
        final Employee result = employeeServiceImplUnderTest.getEmployeeById(0L);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetEmployeeById_EmployeeRepositoryReturnsAbsent() {
        // Setup
        when(mockEmployeeRepository.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> employeeServiceImplUnderTest.getEmployeeById(0L)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testCalculateBhxh() {
        // Setup
        final ConfigInsuranceEntity configInsuranceEntity = new ConfigInsuranceEntity();
        configInsuranceEntity.setId(0L);
        configInsuranceEntity.setPremiumRate(0.0f);
        configInsuranceEntity.setIsNomal(0);
        configInsuranceEntity.setIsFamily(0);
        configInsuranceEntity.setPeopleNum(0);
        configInsuranceEntity.setIsStudent(0);
        configInsuranceEntity.setIsPoorHousehold(0);

        // Configure BaseSalaryRepository.findById(...).
        final BaseSalary baseSalary1 = new BaseSalary();
        baseSalary1.setId(0L);
        baseSalary1.setBaseSalary(0L);
        final Optional<BaseSalary> baseSalary = Optional.of(baseSalary1);
        when(mockBaseSalaryRepository.findById(0L)).thenReturn(baseSalary);

        // Configure ConfigInsuranceRepository.findById(...).
        final ConfigInsuranceEntity configInsuranceEntity2 = new ConfigInsuranceEntity();
        configInsuranceEntity2.setId(0L);
        configInsuranceEntity2.setPremiumRate(0.0f);
        configInsuranceEntity2.setIsNomal(0);
        configInsuranceEntity2.setIsFamily(0);
        configInsuranceEntity2.setPeopleNum(0);
        configInsuranceEntity2.setIsStudent(0);
        configInsuranceEntity2.setIsPoorHousehold(0);
        final Optional<ConfigInsuranceEntity> configInsuranceEntity1 = Optional.of(configInsuranceEntity2);
        when(mockConfigInsuranceRepository.findById(0L)).thenReturn(configInsuranceEntity1);

        // Run the test
        final double result = employeeServiceImplUnderTest.calculateBhxh(0L, configInsuranceEntity);

        // Verify the results
        assertThat(result).isEqualTo(0.0, within(0.0001));
    }

    @Test
    void testCalculateBhxh_BaseSalaryRepositoryReturnsAbsent() {
        // Setup
        final ConfigInsuranceEntity configInsuranceEntity = new ConfigInsuranceEntity();
        configInsuranceEntity.setId(0L);
        configInsuranceEntity.setPremiumRate(0.0f);
        configInsuranceEntity.setIsNomal(0);
        configInsuranceEntity.setIsFamily(0);
        configInsuranceEntity.setPeopleNum(0);
        configInsuranceEntity.setIsStudent(0);
        configInsuranceEntity.setIsPoorHousehold(0);

        when(mockBaseSalaryRepository.findById(0L)).thenReturn(Optional.empty());

        // Configure ConfigInsuranceRepository.findById(...).
        final ConfigInsuranceEntity configInsuranceEntity2 = new ConfigInsuranceEntity();
        configInsuranceEntity2.setId(0L);
        configInsuranceEntity2.setPremiumRate(0.0f);
        configInsuranceEntity2.setIsNomal(0);
        configInsuranceEntity2.setIsFamily(0);
        configInsuranceEntity2.setPeopleNum(0);
        configInsuranceEntity2.setIsStudent(0);
        configInsuranceEntity2.setIsPoorHousehold(0);
        final Optional<ConfigInsuranceEntity> configInsuranceEntity1 = Optional.of(configInsuranceEntity2);
        when(mockConfigInsuranceRepository.findById(0L)).thenReturn(configInsuranceEntity1);

        // Run the test
        final double result = employeeServiceImplUnderTest.calculateBhxh(0L, configInsuranceEntity);

        // Verify the results
        assertThat(result).isEqualTo(0.0, within(0.0001));
    }

    @Test
    void testCalculateBhxh_ConfigInsuranceRepositoryReturnsAbsent() {
        // Setup
        final ConfigInsuranceEntity configInsuranceEntity = new ConfigInsuranceEntity();
        configInsuranceEntity.setId(0L);
        configInsuranceEntity.setPremiumRate(0.0f);
        configInsuranceEntity.setIsNomal(0);
        configInsuranceEntity.setIsFamily(0);
        configInsuranceEntity.setPeopleNum(0);
        configInsuranceEntity.setIsStudent(0);
        configInsuranceEntity.setIsPoorHousehold(0);

        // Configure BaseSalaryRepository.findById(...).
        final BaseSalary baseSalary1 = new BaseSalary();
        baseSalary1.setId(0L);
        baseSalary1.setBaseSalary(0L);
        final Optional<BaseSalary> baseSalary = Optional.of(baseSalary1);
        when(mockBaseSalaryRepository.findById(0L)).thenReturn(baseSalary);

        when(mockConfigInsuranceRepository.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        final double result = employeeServiceImplUnderTest.calculateBhxh(0L, configInsuranceEntity);

        // Verify the results
        assertThat(result).isEqualTo(0.0, within(0.0001));
    }

    @Test
    void testSaveOrUpdate() {
        // Setup
        final Employee employee = new Employee();
        employee.setId(0L);
        employee.setName("name");
        employee.setSalary(0.0);

        final Employee expectedResult = new Employee();
        expectedResult.setId(0L);
        expectedResult.setName("name");
        expectedResult.setSalary(0.0);

        when(mockEmployeeRepository.existsById(0L)).thenReturn(false);

        // Configure EmployeeRepository.save(...).
        final Employee employee1 = new Employee();
        employee1.setId(0L);
        employee1.setName("name");
        employee1.setSalary(0.0);
        when(mockEmployeeRepository.save(new Employee())).thenReturn(employee1);

        // Run the test
        final Employee result = employeeServiceImplUnderTest.saveOrUpdate(0L, employee);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
        verify(mockEmployeeRepository).save(new Employee());
    }

    @Test
    void testDeleteEmployee() {
        // Setup
        when(mockEmployeeRepository.existsById(0L)).thenReturn(false);

        // Run the test
        final boolean result = employeeServiceImplUnderTest.deleteEmployee(0L);

        // Verify the results
        assertThat(result).isFalse();
        verify(mockEmployeeRepository).deleteById(0L);
    }
}
