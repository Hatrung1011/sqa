package com.example.SQA.service.impl;

import com.example.SQA.entity.BaseSalary;
import com.example.SQA.repository.BaseSalaryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BaseSalaryServicesImplTest {

    @Mock
    private BaseSalaryRepository mockBaseSalaryRepository;

    @InjectMocks
    private BaseSalaryServicesImpl baseSalaryServicesImplUnderTest;

    @Test
    void testGetAllSalary() {
        // Setup
        final BaseSalary baseSalary = new BaseSalary();
        baseSalary.setId(0L);
        baseSalary.setBaseSalary(0L);
        final List<BaseSalary> expectedResult = Arrays.asList(baseSalary);

        // Configure BaseSalaryRepository.findAll(...).
        final BaseSalary baseSalary1 = new BaseSalary();
        baseSalary1.setId(0L);
        baseSalary1.setBaseSalary(0L);
        final List<BaseSalary> baseSalaries = Arrays.asList(baseSalary1);
        when(mockBaseSalaryRepository.findAll()).thenReturn(baseSalaries);

        // Run the test
        final List<BaseSalary> result = baseSalaryServicesImplUnderTest.getAllSalary();

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetAllSalary_BaseSalaryRepositoryReturnsNoItems() {
        // Setup
        when(mockBaseSalaryRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<BaseSalary> result = baseSalaryServicesImplUnderTest.getAllSalary();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testChangeBaseSalary() {
        // Setup
        final BaseSalary baseSalary = new BaseSalary();
        baseSalary.setId(0L);
        baseSalary.setBaseSalary(0L);

        when(mockBaseSalaryRepository.existsById(0L)).thenReturn(false);

        // Configure BaseSalaryRepository.save(...).
        final BaseSalary baseSalary1 = new BaseSalary();
        baseSalary1.setId(0L);
        baseSalary1.setBaseSalary(0L);
        when(mockBaseSalaryRepository.save(new BaseSalary())).thenReturn(baseSalary1);

        // Run the test
        final boolean result = baseSalaryServicesImplUnderTest.changeBaseSalary(baseSalary);

        // Verify the results
        assertThat(result).isFalse();
        verify(mockBaseSalaryRepository).save(new BaseSalary());
    }

    @Test
    void testGetSalaryById() {
        // Setup
        final BaseSalary expectedResult = new BaseSalary();
        expectedResult.setId(0L);
        expectedResult.setBaseSalary(0L);

        // Configure BaseSalaryRepository.findById(...).
        final BaseSalary baseSalary1 = new BaseSalary();
        baseSalary1.setId(0L);
        baseSalary1.setBaseSalary(0L);
        final Optional<BaseSalary> baseSalary = Optional.of(baseSalary1);
        when(mockBaseSalaryRepository.findById(0L)).thenReturn(baseSalary);

        // Run the test
        final BaseSalary result = baseSalaryServicesImplUnderTest.getSalaryById(0L);

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetSalaryById_BaseSalaryRepositoryReturnsAbsent() {
        // Setup
        when(mockBaseSalaryRepository.findById(0L)).thenReturn(Optional.empty());

        // Run the test
        final BaseSalary result = baseSalaryServicesImplUnderTest.getSalaryById(0L);

        // Verify the results
        assertThat(result).isNull();
    }

    @Test
    void testDeleteSalary() {
        // Setup
        when(mockBaseSalaryRepository.existsById(0L)).thenReturn(false);

        // Run the test
        final boolean result = baseSalaryServicesImplUnderTest.deleteSalary(0L);

        // Verify the results
        assertThat(result).isFalse();
        verify(mockBaseSalaryRepository).deleteById(0L);
    }
}
