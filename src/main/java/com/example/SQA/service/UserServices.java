package com.example.SQA.service;

import com.example.SQA.model.BaseResponse;
import com.example.SQA.model.LoginDataModel;
import com.example.SQA.model.LoginModel;
import com.example.SQA.model.SignUpModel;

public interface UserServices {
    LoginDataModel login(LoginModel loginModel);

    BaseResponse signup(SignUpModel signUpModel);
}
