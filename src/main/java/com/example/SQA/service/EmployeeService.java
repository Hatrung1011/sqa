package com.example.SQA.service;

import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployees();

    Employee createEmployee(Employee employee);

    Employee getEmployeeById(Long id);

    double calculateBhxh(Long salary, ConfigInsuranceEntity configInsuranceEntity);

    Employee saveOrUpdate(Long id, Employee employee);

    boolean deleteEmployee(Long id);
}
