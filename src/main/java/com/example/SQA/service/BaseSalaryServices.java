package com.example.SQA.service;

import com.example.SQA.entity.BaseSalary;

import java.util.List;

public interface BaseSalaryServices {
    List<BaseSalary> getAllSalary();

    boolean changeBaseSalary(BaseSalary baseSalary);

    BaseSalary getSalaryById(Long id);

    boolean deleteSalary(Long id);
}
