package com.example.SQA.service.impl;

import com.example.SQA.entity.BaseSalary;
import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.entity.Employee;
import com.example.SQA.repository.BaseSalaryRepository;
import com.example.SQA.repository.ConfigInsuranceRepository;
import com.example.SQA.repository.EmployeeRepository;
import com.example.SQA.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ConfigInsuranceRepository configInsuranceRepository;

    @Autowired
    private BaseSalaryRepository baseSalaryRepository;

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Employee getEmployeeById(Long id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isPresent()) {
            return optionalEmployee.get();
        } else {
            throw new RuntimeException("Employee not found!");
        }
    }

    public double calculateBhxh(Long salary, ConfigInsuranceEntity configInsuranceEntity) {
        double bhyt;
        double baseSalary = baseSalaryRepository.findById(1L).orElse(new BaseSalary()).getBaseSalary();
        if (salary < 0) {
            return 0;
        } else if (salary > 20 * baseSalary) {
            bhyt = 0.015 * 20 * baseSalary;
            return (int) Math.ceil(bhyt);
        } else {
            if (salary < baseSalary) {
                bhyt = 0;
            } else {
                Optional<ConfigInsuranceEntity> configInsurance = configInsuranceRepository.findById(configInsuranceEntity.getId());
                if (!configInsurance.isPresent()) {
                    return 0;
                }
                double premiumRate = configInsuranceRepository.findById(configInsuranceEntity.getId()).orElse(new ConfigInsuranceEntity()).getPremiumRate();
                if (configInsuranceRepository.findById(configInsuranceEntity.getId()).orElse(new ConfigInsuranceEntity()).getIsNomal() == 1) {
                    bhyt = salary * premiumRate;
                } else {
                    bhyt = baseSalary * premiumRate;
                }
            }
            return (int) Math.ceil(bhyt);
        }
    }

    @Override
    public Employee saveOrUpdate(Long id, Employee employee) {
        if (employeeRepository.existsById(id)) {
            employeeRepository.save(employee);
        }
        return null;
    }

    @Override
    public boolean deleteEmployee(Long id) {
        boolean flag = false;
        if (employeeRepository.existsById(id)) {
            employeeRepository.deleteById(id);
            flag = true;
        }
        return flag;
    }
}
