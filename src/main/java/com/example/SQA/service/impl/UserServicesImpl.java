package com.example.SQA.service.impl;

import com.example.SQA.entity.Users;
import com.example.SQA.model.BaseResponse;
import com.example.SQA.model.LoginDataModel;
import com.example.SQA.model.LoginModel;
import com.example.SQA.model.SignUpModel;
import com.example.SQA.repository.UserRepository;
import com.example.SQA.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServicesImpl implements UserServices {
    @Autowired
    private UserRepository userRepository;

    @Override
    public LoginDataModel login(LoginModel loginModel) {
        Users users = userRepository.findByUsername(loginModel.getUsername());

        if (users == null) {
            return new LoginDataModel(0, "User is not exist", "");
        }
        if (!users.getPassword().equals(loginModel.getPassword())) {
            return new LoginDataModel(0, "Password is not correct", "");
        }
        return new LoginDataModel(1, "Login success", users.getHoTen());
    }

    @Override
    public BaseResponse signup(SignUpModel signUpModel) {
        Users users = userRepository.findByUsername(signUpModel.getUsername());

        if (users != null) {
            return new BaseResponse(0, "User is alreadly taken");
        } else if (!signUpModel.getPassword().equals(signUpModel.getConfirmPass())) {
            return new BaseResponse(0, "Password and confirm password does not matching");
        }
        else {
            Users tmp = new Users();
            tmp.setUsername(signUpModel.getUsername());
            tmp.setPassword(signUpModel.getPassword());
            tmp.setRole("user");
            tmp.setHoTen(signUpModel.getName());
            tmp.setIsActive(1);
            tmp.setIsDelete(0);
            userRepository.save(tmp);
            return new BaseResponse(1, "Sign up success");
        }
    }
}
