package com.example.SQA.service.impl;

import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.repository.ConfigInsuranceRepository;
import com.example.SQA.service.ConfigInsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConfigInsuranceServiceImpl implements ConfigInsuranceService {
    @Autowired
    private ConfigInsuranceRepository configInsuranceRepository;

    @Override
    public List<ConfigInsuranceEntity> getAllConfig() {
        return configInsuranceRepository.findAll();
    }

    @Override
    public ConfigInsuranceEntity changeConfig(Long id, ConfigInsuranceEntity configInsuranceEntity) {
        if (configInsuranceRepository.existsById(id)) {
            configInsuranceRepository.save(configInsuranceEntity);
        }
        return null;
    }

    @Override
    public ConfigInsuranceEntity getCongigById(Long idVung) {
        Optional<ConfigInsuranceEntity> optionalConfigInsuranceEntity = configInsuranceRepository.findById(idVung);
        if (optionalConfigInsuranceEntity.isPresent()) {
            return optionalConfigInsuranceEntity.get();
        } else {
            throw new RuntimeException("Config not found!");
        }
    }
}
