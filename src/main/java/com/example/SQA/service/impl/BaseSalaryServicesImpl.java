package com.example.SQA.service.impl;

import com.example.SQA.entity.BaseSalary;
import com.example.SQA.model.BaseResponse;
import com.example.SQA.repository.BaseSalaryRepository;
import com.example.SQA.service.BaseSalaryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BaseSalaryServicesImpl implements BaseSalaryServices {
    @Autowired
    private BaseSalaryRepository baseSalaryRepository;

    @Override
    public List<BaseSalary> getAllSalary() {
        return baseSalaryRepository.findAll();
    }

    @Override
    public boolean changeBaseSalary(BaseSalary baseSalary) {
        boolean flag = false;
        if (baseSalaryRepository.existsById(baseSalary.getId())) {
            baseSalaryRepository.save(baseSalary);
            flag=true;
        }
        return flag;
    }

    @Override
    public BaseSalary getSalaryById(Long id) {
        Optional<BaseSalary> baseSalary = baseSalaryRepository.findById(id);
        if (baseSalary.isPresent()) {
            return baseSalary.get();
        }
        return null;
    }

    @Override
    public boolean deleteSalary(Long id) {
        boolean flag = false;
        if (baseSalaryRepository.existsById(id)) {
            baseSalaryRepository.deleteById(id);
            flag = true;
        }
        return flag;
    }


}
