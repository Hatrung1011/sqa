package com.example.SQA.service;

import com.example.SQA.entity.ConfigInsuranceEntity;

import java.util.List;

public interface ConfigInsuranceService {
    List<ConfigInsuranceEntity> getAllConfig();

    ConfigInsuranceEntity changeConfig(Long id, ConfigInsuranceEntity configInsuranceEntity);

    ConfigInsuranceEntity getCongigById(Long idVung);
}
