package com.example.SQA.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "config")
public class ConfigInsuranceEntity implements Serializable {
    private static final long serialVersionUID = -3542589964153392334L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "config_id_seq")
    @SequenceGenerator(sequenceName = "config_id_seq", initialValue = 1, allocationSize = 1, name = "config_id_seq")
    protected Long id;

    @Column(name = "premium_rate")
    private float premiumRate;

    @Column(name = "is_nomal")
    private int isNomal;

    @Column(name = "is_family")
    private int isFamily;

    @Column(name = "people_num")
    private int peopleNum;

    @Column(name = "is_student")
    private int isStudent;

    @Column(name = "is_poor_household")
    private int isPoorHousehold;
}
