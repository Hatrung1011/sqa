package com.example.SQA.entity;

import lombok.Data;
import org.springframework.context.annotation.Role;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user", schema = "public")
public class Users implements Serializable {
    private static final long serialVersionUID = -3542589964153392334L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
    @SequenceGenerator(sequenceName = "user_id_seq", initialValue = 1, allocationSize = 1, name = "user_id_seq")
    protected Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "is_active")
    private int isActive;

    @Column(name = "is_delete")
    private int isDelete;

    @Column(name ="role")
    private String role;

    @Column(name="ho_ten")
    private String hoTen;
}
