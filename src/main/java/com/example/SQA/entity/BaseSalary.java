package com.example.SQA.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "base_salary_config")
public class BaseSalary implements Serializable {
    private static final long serialVersionUID = -3542589964153392334L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "base_salary_config_id_seq")
    @SequenceGenerator(sequenceName = "base_salary_config_id_seq", initialValue = 1, allocationSize = 1, name = "base_salary_config_id_seq")
    protected Long id;

    @Column(name = "base_salary")
    private Long baseSalary;
}
