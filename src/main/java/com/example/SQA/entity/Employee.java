package com.example.SQA.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "employees")
public class Employee implements Serializable {
    private static final long serialVersionUID = -3542589964153392334L;

    @Id
    @Column(name = "idemployees", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employees_idemployees_seq")
    @SequenceGenerator(sequenceName = "employees_idemployees_seq", initialValue = 1, allocationSize = 1, name = "employees_idemployees_seq")
    protected Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "salary")
    private double salary;


}
