package com.example.SQA.job;

import com.example.SQA.service.EmployeeService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.util.Date;

public class KeepAliveJob implements Job {

    private final EmployeeService employeeService;

    public KeepAliveJob(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public void execute(JobExecutionContext context) {
        employeeService.getAllEmployees();
        System.out.println("Ghi log: Job chạy vào lúc: " + new Date());
    }
}
