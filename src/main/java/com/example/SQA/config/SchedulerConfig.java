package com.example.SQA.config;

import com.example.SQA.job.KeepAliveJob;
import org.springframework.context.annotation.Configuration;
import org.quartz.*;
import org.springframework.context.annotation.Bean;

@Configuration
public class SchedulerConfig {
    @Bean
    public JobDetail logJobDetail() {
        return JobBuilder.newJob(KeepAliveJob.class)
                .withIdentity("logJob")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger logJobTrigger() {
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(120)
                .repeatForever();

        return TriggerBuilder.newTrigger()
                .forJob(logJobDetail())
                .withIdentity("logJobTrigger")
                .withSchedule(scheduleBuilder)
                .build();
    }
}
