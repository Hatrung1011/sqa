package com.example.SQA.repository;

import com.example.SQA.entity.ConfigInsuranceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigInsuranceRepository extends JpaRepository<ConfigInsuranceEntity, Long> {
}
