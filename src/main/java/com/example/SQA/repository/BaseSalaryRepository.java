package com.example.SQA.repository;

import com.example.SQA.entity.BaseSalary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseSalaryRepository extends JpaRepository<BaseSalary, Long> {
}
