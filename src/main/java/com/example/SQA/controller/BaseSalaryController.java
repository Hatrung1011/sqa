package com.example.SQA.controller;

import com.example.SQA.entity.BaseSalary;
import com.example.SQA.model.BaseResponse;
import com.example.SQA.service.BaseSalaryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/base_salary")
@CrossOrigin("*")
public class BaseSalaryController {
    @Autowired
    private BaseSalaryServices baseSalaryServices;

    @GetMapping("/get_base_salary")
    public List<BaseSalary> getBaseSalary() {
        return baseSalaryServices.getAllSalary();
    }

    @PostMapping("/change_base_salary")
    public BaseResponse changeBaseSalary(@RequestBody BaseSalary baseSalary) {
        boolean flag = baseSalaryServices.changeBaseSalary(baseSalary);
        if (!flag) {
            return new BaseResponse(0, "Change base salary fail");
        }
        return new BaseResponse(1, "Change base salary success");
    }
}
