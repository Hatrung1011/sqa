package com.example.SQA.controller;

import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.service.ConfigInsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/config")
@CrossOrigin("*")
public class ConfigInsuranceController {
    @Autowired
    private ConfigInsuranceService configInsuranceService;

    @GetMapping("/get-config")
    public List<ConfigInsuranceEntity> getConfig() {
        return configInsuranceService.getAllConfig();
    }

    @PostMapping("/change-config")
    public ResponseEntity<ConfigInsuranceEntity> changeConfig(@RequestParam Long id, @RequestBody ConfigInsuranceEntity configInsuranceEntity) {
        ConfigInsuranceEntity changeConfigInsuranceEntity = configInsuranceService.changeConfig(id, configInsuranceEntity);
        return new ResponseEntity<>(changeConfigInsuranceEntity, HttpStatus.OK);
    }
}
