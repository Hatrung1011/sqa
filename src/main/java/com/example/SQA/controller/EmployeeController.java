package com.example.SQA.controller;

import com.example.SQA.entity.ConfigInsuranceEntity;
import com.example.SQA.entity.Employee;
import com.example.SQA.model.BaseResponse;
import com.example.SQA.service.ConfigInsuranceService;
import com.example.SQA.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
@CrossOrigin("*")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ConfigInsuranceService configInsuranceService;

    @GetMapping("/getAllEmployee")
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @PostMapping("/employee/add")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        Employee savedEmployee = employeeService.createEmployee(employee);
        return new ResponseEntity<>(savedEmployee, HttpStatus.OK);
    }

    @PostMapping("/employee")
    public ResponseEntity<Employee> saveOrUpdate(@RequestParam Long id, @RequestBody Employee employee) {
        Employee savedEmployee = employeeService.saveOrUpdate(id, employee);
        return new ResponseEntity<>(savedEmployee, HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public BaseResponse deleteEmployee(@RequestParam Long id) {
        if (!employeeService.deleteEmployee(id)) {
            return new BaseResponse(0, "Can not delete employee");
        }
        return new BaseResponse(1,"Employee is deleted");
    }

    @GetMapping("/bhxh/config")
    public double calculateBhxh(@RequestParam("salary") Long salary, @RequestParam("idConfig") Long idConfig) {
        ConfigInsuranceEntity configInsuranceEntity = configInsuranceService.getCongigById(idConfig);
        return employeeService.calculateBhxh(salary, configInsuranceEntity);
    }
}
