package com.example.SQA.controller;

import com.example.SQA.model.BaseResponse;
import com.example.SQA.model.LoginDataModel;
import com.example.SQA.model.LoginModel;
import com.example.SQA.model.SignUpModel;
import com.example.SQA.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/auth")

public class UsersController {
    @Autowired
    private UserServices userServices;

    @PostMapping("/login")
    public LoginDataModel login(@RequestBody LoginModel loginModel) {
        return userServices.login(loginModel);
    }

    @PostMapping("/signup")
    public BaseResponse signup(@RequestBody SignUpModel signUpModel) {
        return userServices.signup(signUpModel);
    }
}
