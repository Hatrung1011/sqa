package com.example.SQA.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse implements Serializable {
    private static final long serialVersionUID = -3542589964153392334L;

    private int code;
    private String mess;
}
